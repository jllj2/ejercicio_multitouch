//
//  ViewController.swift
//  ejercicio_multitouch
//
//  Created by mastermoviles on 21/11/18.
//  Copyright © 2018 EPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.imageView.isUserInteractionEnabled = true
        self.imageView.isMultipleTouchEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(handleTap))
        tap.delegate = self
        self.imageView.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action:#selector(handlePan))
        pan.delegate = self
        self.imageView.addGestureRecognizer(pan)
        
        // Gesto de rotación
        let rotation = UIRotationGestureRecognizer(target: self, action:#selector(handleRotation))
        rotation.delegate = self
        self.imageView.addGestureRecognizer(rotation)

        // Gesto de pellizcar
        let pinch = UIPinchGestureRecognizer(target: self, action:#selector(handlePinch))
        pinch.delegate = self
        self.imageView.addGestureRecognizer(pinch)
    }
    @objc func handleTap(sender: UITapGestureRecognizer) {
        print("Tap");
    }
    @objc func handlePan(sender: UIPanGestureRecognizer) {
        print("Pan")
        let translation = sender.translation(in: self.view)
        if let view = sender.view {
            view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
        }
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @objc func handleRotation(sender: UIRotationGestureRecognizer) {
        print("Rotation")
        if let view = sender.view {
            view.transform = view.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        }
    }
    
    @objc func handlePinch(sender: UIPinchGestureRecognizer) {
        print("Pinch")
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            sender.scale = 1;
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    @IBAction func onLongPress(_ sender: Any) {
        print("long press")
    }
}

